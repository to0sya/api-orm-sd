﻿using System;
using System.Collections.Generic;
using CarServiceAPI.Models;
using Microsoft.EntityFrameworkCore;
using Npgsql;

namespace CarServiceAPI.Contexts;

public partial class CarServiceContext : DbContext
{
    public CarServiceContext()
    {

    }

    public CarServiceContext(DbContextOptions<CarServiceContext> options)
        : base(options)
    {
    }

    public virtual DbSet<Car> Cars { get; set; }

    public virtual DbSet<CarBrand> CarBrands { get; set; }

    public virtual DbSet<CarModel> CarModels { get; set; }

    public virtual DbSet<Customer> Customers { get; set; }

    public virtual DbSet<Division> Divisions { get; set; }

    public virtual DbSet<Employee> Employees { get; set; }

    public virtual DbSet<Engine> Engines { get; set; }

    public virtual DbSet<OrderItem> OrderItems { get; set; }

    public virtual DbSet<Ordering> Orderings { get; set; }

    public virtual DbSet<Person> People { get; set; }

    public virtual DbSet<Service> Services { get; set; }

    public virtual DbSet<Site> Sites { get; set; }

    public virtual DbSet<SiteBusyHour> SiteBusyHours { get; set; }

    public virtual DbSet<Skill> Skills { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        => optionsBuilder.UseNpgsql("Host=localhost;Port=5432;Database=CarService;Username=postgres;Password=1111");

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder
            .HasPostgresEnum("aspiration", new[] { "TURBOCHARGED", "N/A" })
            .HasPostgresEnum("fuel", new[] { "Gasoline", "Diesel", "Electricity", "Gasoline/Propane", "Hybrid" });

        modelBuilder.Entity<Car>(entity =>
        {
            entity.HasKey(e => e.VinCode).HasName("car_pkey");

            entity.ToTable("car");

            entity.Property(e => e.VinCode)
                .HasMaxLength(17)
                .HasColumnName("vin_code");
            entity.Property(e => e.BrandName)
                .HasMaxLength(30)
                .HasColumnName("brand_name");
            entity.Property(e => e.ModelId).HasColumnName("model_id");

            entity.HasOne(d => d.BrandNameNavigation).WithMany(p => p.Cars)
                .HasForeignKey(d => d.BrandName)
                .HasConstraintName("car_brand_name_fkey");

            entity.HasOne(d => d.Model).WithMany(p => p.Cars)
                .HasForeignKey(d => d.ModelId)
                .HasConstraintName("car_model_id_fkey");
        });

        modelBuilder.Entity<CarBrand>(entity =>
        {
            entity.HasKey(e => e.Name).HasName("car_brand_pkey");

            entity.ToTable("car_brand");

            entity.Property(e => e.Name)
                .HasMaxLength(30)
                .HasColumnName("name");
            entity.Property(e => e.GroupName)
                .HasMaxLength(50)
                .HasColumnName("group_name");
        });

        modelBuilder.Entity<CarModel>(entity =>
        {
            entity.HasKey(e => e.ModelId).HasName("car_model_pkey");

            entity.ToTable("car_model");

            entity.Property(e => e.ModelId).HasColumnName("model_id");
            entity.Property(e => e.BrandName)
                .HasMaxLength(30)
                .HasColumnName("brand");
            entity.Property(e => e.EngineId).HasColumnName("engine_id");
            entity.Property(e => e.ModelName)
                .HasMaxLength(20)
                .HasColumnName("model_name");

            entity.HasOne(d => d.BrandNameNavigation).WithMany(p => p.CarModels)
                .HasForeignKey(d => d.BrandName)
                .HasConstraintName("car_model_brand_name_fkey");

            entity.HasOne(d => d.Engine).WithMany(p => p.CarModels)
                .HasForeignKey(d => d.EngineId)
                .HasConstraintName("car_model_engine_id_fkey");
        });

        modelBuilder.Entity<Customer>(entity =>
        {
            entity.HasKey(e => e.CustomerId).HasName("customer_pkey");

            entity.ToTable("customer");

            entity.HasIndex(e => e.PersonId, "customer_person_id_key").IsUnique();

            entity.Property(e => e.CustomerId).HasColumnName("customer_id");
            entity.Property(e => e.PersonId).HasColumnName("person_id");
            entity.Property(e => e.PhoneNumber)
                .HasMaxLength(13)
                .HasColumnName("phone_number");

            entity.HasOne(d => d.Person).WithOne(p => p.Customer)
                .HasForeignKey<Customer>(d => d.PersonId)
                .HasConstraintName("customer_person_id_fkey");

            

            entity.HasMany(d => d.VinCodes).WithMany(p => p.Customers)
                .UsingEntity<Dictionary<string, object>>(
                    "CustomerCar",
                    r => r.HasOne<Car>().WithMany()
                        .HasForeignKey("VinCode")
                        .OnDelete(DeleteBehavior.ClientSetNull)
                        .HasConstraintName("customer_car_vin_code_fkey"),
                    l => l.HasOne<Customer>().WithMany()
                        .HasForeignKey("CustomerId")
                        .OnDelete(DeleteBehavior.ClientSetNull)
                        .HasConstraintName("customer_car_customer_id_fkey"),
                    j =>
                    {
                        j.HasKey("CustomerId", "VinCode").HasName("customer_car_pkey");
                        j.ToTable("customer_car");
                        j.IndexerProperty<int>("CustomerId").HasColumnName("customer_id");
                        j.IndexerProperty<string>("VinCode")
                            .HasMaxLength(17)
                            .HasColumnName("vin_code");
                    });
        });

        modelBuilder.Entity<Division>(entity =>
        {
            entity.HasKey(e => e.DivisionId).HasName("division_pkey");

            entity.ToTable("division");

            entity.Property(e => e.DivisionId).HasColumnName("division_id");
            entity.Property(e => e.ClosingTime).HasColumnName("closing_time");
            entity.Property(e => e.Location)
                .HasMaxLength(100)
                .HasColumnName("location");
            entity.Property(e => e.Name)
                .HasMaxLength(50)
                .HasColumnName("name");
            entity.Property(e => e.OpeningTime).HasColumnName("opening_time");
        });

        modelBuilder.Entity<Employee>(entity =>
        {
            entity.HasKey(e => e.EmployeeId).HasName("employee_pkey");

            entity.ToTable("employee");

            entity.HasIndex(e => e.PersonId, "employee_person_id_key").IsUnique();

            entity.Property(e => e.EmployeeId).HasColumnName("employee_id");
            entity.Property(e => e.DivisionId).HasColumnName("division_id");
            entity.Property(e => e.PersonId).HasColumnName("person_id");
            entity.Property(e => e.Position)
                .HasMaxLength(50)
                .HasColumnName("position");

            entity.HasOne(d => d.Division).WithMany(p => p.Employees)
                .HasForeignKey(d => d.DivisionId)
                .HasConstraintName("employee_division_id_fkey");

            entity.HasOne(d => d.Person).WithOne(p => p.Employee)
                .HasForeignKey<Employee>(d => d.PersonId)
                .HasConstraintName("employee_person_id_fkey");

            entity.HasMany(d => d.Sites).WithMany(p => p.Employees)
                .UsingEntity<Dictionary<string, object>>(
                    "SiteMaintainer",
                    r => r.HasOne<Site>().WithMany()
                        .HasForeignKey("SiteId")
                        .OnDelete(DeleteBehavior.ClientSetNull)
                        .HasConstraintName("site_maintainer_site_id_fkey"),
                    l => l.HasOne<Employee>().WithMany()
                        .HasForeignKey("EmployeeId")
                        .OnDelete(DeleteBehavior.ClientSetNull)
                        .HasConstraintName("site_maintainer_employee_id_fkey"),
                    j =>
                    {
                        j.HasKey("EmployeeId", "SiteId").HasName("site_maintainer_pkey");
                        j.ToTable("site_maintainer");
                        j.IndexerProperty<int>("EmployeeId").HasColumnName("employee_id");
                        j.IndexerProperty<int>("SiteId").HasColumnName("site_id");
                    });
        });

        modelBuilder.Entity<Engine>(entity =>
        {
            entity.HasKey(e => e.EngineId).HasName("engine_pkey");

            entity.ToTable("engine");

            entity.Property(e => e.EngineId).HasColumnName("engine_id");
            entity.Property(e => e.Volume).HasColumnName("volume");
        });

        modelBuilder.Entity<OrderItem>(entity =>
        {
            entity.HasKey(e => new { e.OrderId, e.ServiceId }).HasName("order_item_pkey");

            entity.ToTable("order_item");

            entity.Property(e => e.OrderId).HasColumnName("order_id");
            entity.Property(e => e.ServiceId).HasColumnName("service_id");
            entity.Property(e => e.EmployeeId).HasColumnName("employee_id");

            entity.HasOne(d => d.Employee).WithMany(p => p.OrderItems)
                .HasForeignKey(d => d.EmployeeId)
                .HasConstraintName("order_item_employee_id_fkey");

            entity.HasOne(d => d.Order).WithMany(p => p.OrderItems)
                .HasForeignKey(d => d.OrderId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("order_item_order_id_fkey");

            entity.HasOne(d => d.Service).WithMany(p => p.OrderItems)
                .HasForeignKey(d => d.ServiceId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("order_item_service_id_fkey");
        });

        modelBuilder.Entity<Ordering>(entity =>
        {
            entity.HasKey(e => e.OrderId).HasName("ordering_pkey");

            entity.ToTable("ordering");

            entity.Property(e => e.OrderId).HasColumnName("order_id");
            entity.Property(e => e.CustomerId).HasColumnName("customer_id");
            entity.Property(e => e.ManagerId).HasColumnName("manager_id");
            entity.Property(e => e.VinCode)
                .HasMaxLength(17)
                .HasColumnName("vin_code");

            entity.HasOne(d => d.Customer).WithMany(p => p.Orderings)
                .HasForeignKey(d => d.CustomerId)
                .HasConstraintName("ordering_customer_id_fkey");

            entity.HasOne(d => d.Manager).WithMany(p => p.Orderings)
                .HasForeignKey(d => d.ManagerId)
                .HasConstraintName("ordering_manager_id_fkey");

            entity.HasOne(d => d.VinCodeNavigation).WithMany(p => p.Orderings)
                .HasForeignKey(d => d.VinCode)
                .HasConstraintName("ordering_vin_code_fkey");
        });

        modelBuilder.Entity<Person>(entity =>
        {
            entity.HasKey(e => e.PersonId).HasName("person_pkey");

            entity.ToTable("person");

            entity.Property(e => e.PersonId).HasColumnName("person_id");
            entity.Property(e => e.BirthDate).HasColumnName("birth_date");
            entity.Property(e => e.Gender)
                .HasMaxLength(50)
                .HasColumnName("gender");
            entity.Property(e => e.Name)
                .HasMaxLength(50)
                .HasColumnName("name");
            entity.Property(e => e.Surname)
                .HasMaxLength(50)
                .HasColumnName("surname");
        });

        modelBuilder.Entity<Service>(entity =>
        {
            entity.HasKey(e => e.ServiceId).HasName("service_pkey");

            entity.ToTable("service");

            entity.Property(e => e.ServiceId).HasColumnName("service_id");
            entity.Property(e => e.Name)
                .HasMaxLength(50)
                .HasColumnName("name");
            entity.Property(e => e.Price)
                .HasPrecision(9, 2)
                .HasColumnName("price");
            entity.Property(e => e.SkillId).HasColumnName("skill_id");

            entity.HasOne(d => d.Skill).WithMany(p => p.Services)
                .HasForeignKey(d => d.SkillId)
                .HasConstraintName("service_skill_id_fkey");
        });

        modelBuilder.Entity<Site>(entity =>
        {
            entity.HasKey(e => e.SiteId).HasName("site_pkey");

            entity.ToTable("site");

            entity.Property(e => e.SiteId).HasColumnName("site_id");
            entity.Property(e => e.DivisionId).HasColumnName("division_id");

            entity.HasOne(d => d.Division).WithMany(p => p.Sites)
                .HasForeignKey(d => d.DivisionId)
                .HasConstraintName("site_division_id_fkey");
        });

        modelBuilder.Entity<SiteBusyHour>(entity =>
        {
            entity.HasKey(e => e.SiteBusyHoursId).HasName("site_busy_hours_pkey");

            entity.ToTable("site_busy_hours");

            entity.Property(e => e.SiteBusyHoursId).HasColumnName("site_busy_hours_id");
            entity.Property(e => e.Date).HasColumnName("date");
            entity.Property(e => e.EndTime).HasColumnName("end_time");
            entity.Property(e => e.OrderId).HasColumnName("order_id");
            entity.Property(e => e.ServiceId).HasColumnName("service_id");
            entity.Property(e => e.SiteId).HasColumnName("site_id");
            entity.Property(e => e.StartTime).HasColumnName("start_time");

            entity.HasOne(d => d.Site).WithMany(p => p.SiteBusyHours)
                .HasForeignKey(d => d.SiteId)
                .HasConstraintName("site_busy_hours_site_id_fkey");

            entity.HasOne(d => d.OrderItem).WithMany(p => p.SiteBusyHours)
                .HasForeignKey(d => new { d.OrderId, d.ServiceId })
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("site_busy_hours_order_id_service_id_fkey");
        });

        modelBuilder.Entity<Skill>(entity =>
        {
            entity.HasKey(e => e.SkillId).HasName("skill_pkey");

            entity.ToTable("skill");

            entity.Property(e => e.SkillId).HasColumnName("skill_id");
            entity.Property(e => e.Name)
                .HasMaxLength(50)
                .HasColumnName("name");

            entity.HasMany(d => d.Employees).WithMany(p => p.Skills)
                .UsingEntity<Dictionary<string, object>>(
                    "EmployeeSkill",
                    r => r.HasOne<Employee>().WithMany()
                        .HasForeignKey("EmployeeId")
                        .OnDelete(DeleteBehavior.ClientSetNull)
                        .HasConstraintName("employee_skill_employee_id_fkey"),
                    l => l.HasOne<Skill>().WithMany()
                        .HasForeignKey("SkillId")
                        .OnDelete(DeleteBehavior.ClientSetNull)
                        .HasConstraintName("employee_skill_skill_id_fkey"),
                    j =>
                    {
                        j.HasKey("SkillId", "EmployeeId").HasName("employee_skill_pkey");
                        j.ToTable("employee_skill");
                        j.IndexerProperty<int>("SkillId").HasColumnName("skill_id");
                        j.IndexerProperty<int>("EmployeeId").HasColumnName("employee_id");
                    });
        });

        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
}
