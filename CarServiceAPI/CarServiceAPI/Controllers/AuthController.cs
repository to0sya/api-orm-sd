﻿using Microsoft.AspNetCore.Mvc;

namespace CarServiceAPI.Controllers
{
    [ApiController]
    [Route("api")]
    public class AuthController : Controller
    {
        [HttpPost("/authenticate")]
        public async Task<IActionResult> Auth(object something)
        {
            Console.WriteLine(something);
            return Ok(1);
        }
    }
}
