﻿using CarServiceAPI.DTOs;
using CarServiceAPI.Models;
using CarServiceAPI.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace CarServiceAPI.Controllers
{
    [ApiController]
    [Route("cars")]
    public class CarsController : Controller
    {
        private readonly IRepository<Car, string> _repository;

        public CarsController(IRepository<Car, string> repo)
        {
            this._repository = repo;
        }

        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            var answer = await _repository.GetAll();

            if (answer != null)
            {
                var carListDto = answer.Select(c => new CarDto()
                {
                    vin_code = c.VinCode,
                    brand_name = c.BrandName,
                    model_name = c.Model!.ModelName,
                    engine_volume = c.Model.Engine!.Volume,
                    fuel_type = c.Model.Engine.Fuel.ToString(),
                    aspiration = c.Model.Engine.Aspiration.ToString()
                }).ToList();

                return Ok(carListDto);
            }
            else
            {
                return NotFound("Car not found");
            }
        }

        [HttpGet("{vincode}")]
        public async Task<IActionResult> Get(string vincode)
        {
            var answer = await _repository.Get(vincode);

            if (answer != null)
            {
                var dto = new CarDto()
                {
                    vin_code = answer.VinCode,
                    brand_name = answer.BrandName,
                    model_name = answer.Model!.ModelName,
                    engine_volume = answer.Model.Engine!.Volume,
                    fuel_type = answer.Model.Engine.Fuel.ToString(),
                    aspiration = answer.Model.Engine.Aspiration.ToString()
                };

                return Ok(dto);
            }
            else
            {
                return NotFound("Car not found");
            }
        }

        [HttpPost]
        public async Task<IActionResult> Add(Car car)
        {
            var answer = await _repository.Add(car);

            return answer != string.Empty 
                ? Ok(new { success = true, customer_id = answer }) 
                : BadRequest(answer);
        }

        [HttpDelete("{vincode}")]
        public async Task<IActionResult> Delete(string vincode)
        {
            var answer = await _repository.Delete(vincode);

            if (answer != 0)
            {
                return Ok(new { success = true });
            } else
            {
                return NotFound(new {success = false});
            }
        }

        [HttpPut("{vincode}")]
        public async Task<IActionResult> UpdateCar(string vincode, Car car)
        {
            if (vincode != car.VinCode)
            {
                return BadRequest(new { success = false, error = "Sent vincode and car vincode does not agree" });
            }

            var answer = await _repository.Update(car);

            if (answer != 0)
            {
                return Ok(new {success = true});
            } else
            {
                return BadRequest(new { success = false });
            }
        }
    }
}
