﻿using Microsoft.AspNetCore.Mvc;
using CarServiceAPI.Models;
using CarServiceAPI.Repositories;
using Npgsql;
using CarServiceAPI.DTOs;

namespace CarServiceAPI.Controllers
{
    [ApiController]
    [Route("customers")]
    public class CustomersController : Controller
    {
        private readonly IRepository<Customer, int> _repository;

        public CustomersController(IRepository<Customer, int> repo) {
            this._repository = repo;
        }

        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            var answer = await _repository.GetAll();

            if (answer != null)
            {
                var dto = answer.Select(c => new CustomerDto()
                                {
                                    id = c.CustomerId,
                                    name = c.Person!.Name,
                                    surname = c.Person!.Surname,
                                    gender = c.Person!.Gender,
                                    birth_date = c.Person!.BirthDate,
                                    phone_number = c.PhoneNumber,
                                    cars = (c.VinCodes as List<Car>)!.Select(car => new CarDto()
                                    {
                                        vin_code = car.VinCode,
                                        brand_name = car.BrandName,
                                        model_name = car.Model!.ModelName,
                                        engine_volume = car.Model.Engine!.Volume,
                                        fuel_type = car.Model.Engine.Fuel.ToString(),
                                        aspiration = car.Model.Engine.Aspiration.ToString()

                                    }).ToList()
                });

                return Ok(dto);
            }
            else
            {
                return NotFound("No customers found");
            }
        }

       

        [HttpGet("{id:int}")]
        public async Task<IActionResult> Get(int id)
        {
            var answer = await _repository.Get(id);

            if (answer != null)
            {
                var dto = new CustomerDto()
                {
                    id = answer.CustomerId,
                    name = answer.Person!.Name,
                    surname = answer.Person!.Surname,
                    gender = answer.Person!.Gender,
                    birth_date = answer.Person!.BirthDate,
                    phone_number = answer.PhoneNumber,
                    cars = (answer.VinCodes as List<Car>)!.Select(car => new CarDto()
                    {
                        vin_code = car.VinCode,
                        brand_name = car.BrandName,
                        model_name = car.Model!.ModelName,
                        engine_volume = car.Model.Engine!.Volume,
                        fuel_type = car.Model.Engine.Fuel.ToString(),
                        aspiration = car.Model.Engine.Aspiration.ToString()

                    }).ToList()
                };


                return Ok(dto);
            }
            else
            {
                return NotFound("Customer not found");
            }
        }

        [HttpPost]
        public async Task<IActionResult> AddCustomer(CustomerDto customer)
        {
            Customer customerModel = new Customer()
            {
                Person = new Person()
                {
                    Name = customer.name,
                    Surname = customer.surname,
                    BirthDate = customer.birth_date,
                    Gender = customer.gender
                },
                PhoneNumber = customer.phone_number,
                VinCodes = customer.cars.Select(c =>
                    new Car()
                    {
                        BrandName = c.brand_name,
                        VinCode = c.vin_code,
                        Model = new CarModel()
                        {
                            ModelName = c.model_name,
                            BrandName = c.brand_name,
                            Engine = new Engine()
                            {
                                Volume = c.engine_volume,
                                Aspiration = Enum.Parse<Aspiration>(c.aspiration),
                                Fuel = Enum.Parse<Fuel>(c.fuel_type)
                            }
                        }
                    }
                ).ToList()
            };

            var answer = await _repository.Add(customerModel);

            return answer != 0 
                ? Ok(new { success = true, customer_id = answer }) 
                : BadRequest(answer);
        }

        [HttpDelete("{id:int}")]
        public async Task<IActionResult> DeleteCustomer(int id)
        {
            var answer = await _repository.Delete(id);

            if (answer != 0)
            {
                return Ok(new { success = true, customer_id = id });
            } else
            {
                return NotFound(new {success = false});
            }
        }

        [HttpPut("{id:int}")]
        public async Task<IActionResult> UpdateCustomer(int id, CustomerDto customer)
        {
            Customer customerModel = new Customer()
            {
                CustomerId = id,
                Person = new Person()
                {
                    PersonId = _repository.Get(id).Result.PersonId,
                    Name = customer.name,
                    Surname = customer.surname,
                    BirthDate = customer.birth_date,
                    Gender = customer.gender
                },
                PhoneNumber = customer.phone_number
            };


            if (id != customerModel.CustomerId)
            {
                return BadRequest(new { success = false, error = "Sent id and customer id does not agree" });
            }

            var answer = await _repository.Update(customerModel);

            if (answer != 0)
            {
                return Ok(new {success = true});
            } else
            {
                return BadRequest(new { success = false });
            }
        }
        
        [HttpGet("/get_days_before_birthday/{id:int}")]
        public async Task<IActionResult> GetDaysUntilBirthday(int id)
        {
            var user = await _repository.Get(id);

            if (user == null)
            {
                return NotFound(new { success = false, error = "Customer not found" });
            }

            var birthDate = user.Person!.BirthDate;

            using (var npgsqlConnection = new NpgsqlConnection("Host=localhost;Port=5432;Database=CarService;Username=postgres;Password=1111"))
            {
                npgsqlConnection.Open();

                using (var command = new NpgsqlCommand("SELECT calculate_days_until_birthday(@birthday)", npgsqlConnection))
                {
                    command.Parameters.AddWithValue("@birthday", birthDate);
                    
                    using (var  reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            return Ok(new {success = true, days_until_birthday = reader["calculate_days_until_birthday"]});
                        }
                        return BadRequest();
                    }
                }
            }
        }
    }
}
