﻿using CarServiceAPI.DTOs;
using CarServiceAPI.Models;
using CarServiceAPI.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace CarServiceAPI.Controllers
{
    [ApiController]
    [Route("employees")]
    public class EmployeesController : Controller
    {
        private readonly IRepository<Employee, int> _repository;

        public EmployeesController(IRepository<Employee, int> repo)
        {
            this._repository = repo;
        }

        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            var answer = await _repository.GetAll();

            if (answer != null)
            {
                var employeeDtoList = answer.Select(e => new EmployeeDto()
                {
                    id = e.EmployeeId,
                    name = e.Person!.Name,
                    surname = e.Person.Surname,
                    gender = e.Person.Gender,
                    birth_date = e.Person.BirthDate,
                    position = e.Position,
                    skills = (e.Skills as List<Skill>)!.Select(s => new SkillDto()
                    {
                        name = s.Name!
                    }).ToList()
                }).ToList();

                return Ok(employeeDtoList);
            }
            else
            {
                return NotFound("There are no employees");
            }
        }

        [HttpGet("{id:int}")]
        public async Task<IActionResult> Get(int id)
        {
            var answer = await _repository.Get(id);

            if (answer != null)
            {
                var employeeDtoList = new EmployeeDto()
                {
                    id = answer.EmployeeId,
                    name = answer.Person!.Name,
                    surname = answer.Person.Surname,
                    gender = answer.Person.Gender,
                    birth_date = answer.Person.BirthDate,
                    position = answer.Position,
                    skills = (answer.Skills as List<Skill>)!.Select(s => new SkillDto()
                    {
                        name = s.Name!
                    }).ToList()
                };

                return Ok(employeeDtoList);
            }
            else
            {
                return NotFound("No employee was found with this id");
            }
        }

        [HttpPost]
        public async Task<IActionResult> Add(Employee employee)
        {
            var answer = await _repository.Add(employee);

            return answer != 0 
                ? Ok(new { success = true, employee_id = answer }) 
                : BadRequest(answer);
        }

        [HttpDelete("{id:int}")]
        public async Task<IActionResult> Delete(int id)
        {
            var answer = await _repository.Delete(id);

            if (answer != 0)
            {
                return Ok(new { success = true, customer_id = id });
            } else
            {
                return NotFound(new {success = false});
            }
        }

        [HttpPut("{id:int}")]
        public async Task<IActionResult> Update(int id, Employee employee)
        {
            if (id != employee.EmployeeId)
            {
                return BadRequest(new { success = false, error = "Sent id and employee id does not agree" });
            }

            var answer = await _repository.Update(employee);

            if (answer != 0)
            {
                return Ok(new {success = true});
            } else
            {
                return BadRequest(new { success = false });
            }
        }
    }
}

