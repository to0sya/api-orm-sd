﻿using CarServiceAPI.DTOs;
using CarServiceAPI.Models;
using CarServiceAPI.Repositories;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace CarServiceAPI.Controllers
{
    [ApiController]
    [Route("orderings")]
    public class OrderingsController : Controller
    {
        private readonly IRepository<Ordering, int> _repository;

        public OrderingsController(IRepository<Ordering, int> repo)
        {
            this._repository = repo;
        }

        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            var answer = await _repository.GetAll();

            if (answer != null)
            {
                var orderingDto = answer.Select(o => new OrderingDto()
                {
                    ordering_id = o.OrderId,
                    car = o.VinCodeNavigation!.BrandName + " " + o.VinCodeNavigation.Model!.ModelName +
                          " " + o.VinCodeNavigation.Model.Engine!.Volume + " " +
                          o.VinCodeNavigation.Model.Engine.Fuel.ToString() +
                          " " + o.VinCodeNavigation.Model.Engine.Aspiration.ToString(),
                    customer = o.Customer!.Person!.Name + " " + o.Customer.Person.Surname,
                    manager = o.Manager!.Person!.Name + " " + o.Manager.Person.Surname,
                    order_items = o.OrderItems.Select(oi => new OrderItemDto()
                    {
                        service = oi.Service!.Name,
                        service_price = oi.Service.Price,
                        employee = oi.Employee!.Person!.Name + " " + oi.Employee.Person.Surname,
                        employee_position = oi.Employee.Position,
                        schedule = oi.SiteBusyHours.Select(sbh => new SiteBusyHourDto()
                        {
                            date = sbh.Date,
                            start_time = sbh.StartTime,
                            end_time = sbh.EndTime
                        }).ToList()

                    }).ToList()
                });

                return Ok(orderingDto);
            }
            else
            {
                return NotFound("Ordering with specified id was not found");
            }
        }

        [HttpGet("{id:int}")]
        public async Task<IActionResult> Get(int id)
        {
            var answer = await _repository.Get(id);

            if (answer != null)
            {
                var orderingDto = new OrderingDto()
                {
                    ordering_id = answer.OrderId,
                    car = answer.VinCodeNavigation!.BrandName + " " + answer.VinCodeNavigation.Model!.ModelName +
                          " " + answer.VinCodeNavigation.Model.Engine!.Volume + " " +
                          answer.VinCodeNavigation.Model.Engine.Fuel.ToString() +
                          " " + answer.VinCodeNavigation.Model.Engine.Aspiration.ToString(),
                    customer = answer.Customer!.Person!.Name + " " + answer.Customer.Person.Surname,
                    manager = answer.Manager!.Person!.Name + " " + answer.Manager.Person.Surname,
                    order_items = answer.OrderItems.Select(oi => new OrderItemDto()
                    {
                        service = oi.Service!.Name,
                        service_price = oi.Service.Price,
                        employee = oi.Employee!.Person!.Name + " " + oi.Employee.Person.Surname,
                        employee_position = oi.Employee.Position,
                        schedule = oi.SiteBusyHours.Select(sbh => new SiteBusyHourDto()
                        {
                            date = sbh.Date,
                            start_time = sbh.StartTime,
                            end_time = sbh.EndTime
                        }).ToList()

                    }).ToList()
                };

                return Ok(orderingDto);
            }
            else
            {
                return NotFound("Ordering with specified id was not found");
            }
        }

        [HttpPost]
        public async Task<IActionResult> Add(Ordering ordering)
        {

            var answer = await _repository.Add(ordering);

            return answer != 0 
                ? Ok(new { success = true, ordering_id = answer }) 
                : BadRequest(answer);
        }

        [HttpDelete("{id:int}")]
        public async Task<IActionResult> Delete(int id)
        {
            var answer = await _repository.Delete(id);

            if (answer != 0)
            {
                return Ok(new { success = true, customer_id = id });
            } else
            {
                return NotFound(new {success = false});
            }
        }

        [HttpPut("{id:int}")]
        public async Task<IActionResult> Update(int id, Ordering ordering)
        {
            if (!id.Equals(ordering.OrderId))
            {
                return BadRequest(JsonConvert.SerializeObject(new { success = false, error = "Sent id and order id does not agree" }));
            }

            var answer = await _repository.Update(ordering);

            if (answer != 0)
            {
                return Ok(new {success = true});
            } else
            {
                return BadRequest(new { success = false });
            }
        }
    }
}
