﻿using CarServiceAPI.Models;
using Humanizer.Localisation.TimeToClockNotation;

namespace CarServiceAPI.DTOs
{
    public class CarDto
    {
        public string? vin_code { get; set; }

        public string? brand_name { get; set; }

        public string? model_name { get; set; }

        public int engine_volume { get; set; }

        public string? fuel_type { get; set; }

        public string? aspiration { get; set; }
    }
}
