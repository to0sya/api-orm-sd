﻿using System.Diagnostics.CodeAnalysis;

namespace CarServiceAPI.DTOs
{
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public class CustomerDto
    {
        public int id { get; set; }

        public string? name { get; set; }

        public string? surname { get; set; }

        public string? gender { get; set; }

        public DateOnly birth_date { get; set; }

        public string? phone_number { get; set; }

        public List<CarDto>? cars { get; set; }
    }
}
