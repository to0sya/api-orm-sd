﻿namespace CarServiceAPI.DTOs;

public class EmployeeDto
{
    public int id { get; set; }
    
    public string name { get; set; }
    
    public string surname { get; set; }
    
    public string gender { get; set; }
    
    public DateOnly birth_date { get; set; }
    
    public string position { get; set; }
    
    public List<SkillDto> skills { get; set; }
}