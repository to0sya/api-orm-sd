﻿namespace CarServiceAPI.DTOs;

public class OrderItemDto
{
    public string service { get; set; }
    
    public decimal service_price { get; set; }
    
    public string employee { get; set; }
    
    public string employee_position { get; set; }
    
    public List<SiteBusyHourDto> schedule { get; set; }
}