﻿using CarServiceAPI.Models;

namespace CarServiceAPI.DTOs;

public class OrderingDto
{
    public int ordering_id { get; set; }
    
    public string car { get; set; }
    
    public string customer { get; set; }
    
    public string manager { get; set; }
    
    public List<OrderItemDto> order_items { get; set; }
}