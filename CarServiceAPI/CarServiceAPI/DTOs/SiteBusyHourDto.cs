﻿namespace CarServiceAPI.DTOs;

public class SiteBusyHourDto
{
    public DateOnly date { get; set; }
    
    public TimeOnly start_time { get; set; }
    
    public TimeOnly end_time { get; set; }
}