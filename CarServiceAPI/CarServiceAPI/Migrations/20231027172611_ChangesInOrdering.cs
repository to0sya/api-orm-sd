﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace CarServiceAPI.Migrations
{
    /// <inheritdoc />
    public partial class ChangesInOrdering : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Comment",
                table: "ordering",
                type: "text",
                nullable: true);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Comment",
                table: "ordering");
        }
    }
}
