﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace CarServiceAPI.Migrations
{
    /// <inheritdoc />
    public partial class updatesInCustomer : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "customer_person_id_fkey",
                table: "customer");

            migrationBuilder.AlterColumn<int>(
                name: "person_id",
                table: "customer",
                type: "integer",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "integer",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "customer_person_id_fkey",
                table: "customer",
                column: "person_id",
                principalTable: "person",
                principalColumn: "person_id",
                onDelete: ReferentialAction.Cascade);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "customer_person_id_fkey",
                table: "customer");

            migrationBuilder.AlterColumn<int>(
                name: "person_id",
                table: "customer",
                type: "integer",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "integer");

            migrationBuilder.AddForeignKey(
                name: "customer_person_id_fkey",
                table: "customer",
                column: "person_id",
                principalTable: "person",
                principalColumn: "person_id");
        }
    }
}
