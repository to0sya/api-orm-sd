﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace CarServiceAPI.Migrations
{
    /// <inheritdoc />
    public partial class OnUpdateCascadeEmployeeSkill : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("alter table public.employee_skill\r\n    drop constraint employee_skill_employee_id_fkey;\r\n\r\nalter table public.employee_skill\r\n    add foreign key (employee_id) references public.employee\r\n        on update cascade on delete cascade;\r\n");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
