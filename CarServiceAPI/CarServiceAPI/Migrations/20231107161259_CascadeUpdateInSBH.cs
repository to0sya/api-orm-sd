﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace CarServiceAPI.Migrations
{
    /// <inheritdoc />
    public partial class CascadeUpdateInSBH : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("alter table public.site_busy_hours\r\n    drop constraint site_busy_hours_order_id_service_id_fkey;\r\n\r\nalter table public.site_busy_hours\r\n    add foreign key (order_id, service_id) references public.order_item\r\n        on update cascade;\r\n\r\n");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
