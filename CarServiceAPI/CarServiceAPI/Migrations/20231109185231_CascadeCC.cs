﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace CarServiceAPI.Migrations
{
    /// <inheritdoc />
    public partial class CascadeCC : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("alter table public.customer_car\r\n    drop constraint customer_car_customer_id_fkey;\r\n\r\nalter table public.customer_car\r\n    add foreign key (customer_id) references public.customer\r\n        on delete cascade;\r\n\r\n");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
