﻿using NpgsqlTypes;

namespace CarServiceAPI.Models
{
    public enum Aspiration
    {
        [PgName("TURBOCHARGED")]
        Turbocharged,
        [PgName("N/A")]
        NaturalAspiration
    }
}
