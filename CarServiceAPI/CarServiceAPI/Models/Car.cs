﻿using System;
using System.Collections.Generic;

namespace CarServiceAPI.Models;

public partial class Car : IEntity
{
    public string VinCode { get; set; } = null!;

    public string? BrandName { get; set; }

    public int? ModelId { get; set; }

    public virtual CarBrand? BrandNameNavigation { get; set; }

    public virtual CarModel? Model { get; set; }

    public virtual ICollection<Ordering> Orderings { get; set; } = new List<Ordering>();

    public virtual ICollection<Customer> Customers { get; set; } = new List<Customer>();
}
