﻿using System;
using System.Collections.Generic;

namespace CarServiceAPI.Models;

public partial class CarBrand : IEntity
{
    public string Name { get; set; } = null!;

    public string? GroupName { get; set; }

    public virtual ICollection<CarModel> CarModels { get; set; } = new List<CarModel>();

    public virtual ICollection<Car> Cars { get; set; } = new List<Car>();
}
