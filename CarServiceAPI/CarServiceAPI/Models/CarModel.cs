﻿using System;
using System.Collections.Generic;

namespace CarServiceAPI.Models;

public partial class CarModel : IEntity
{
    public int ModelId { get; set; }
    
    public string? BrandName { get; set; }

    public string ModelName { get; set; } = null!;

    public int? EngineId { get; set; }

    public virtual CarBrand? BrandNameNavigation { get; set; }

    public virtual ICollection<Car> Cars { get; set; } = new List<Car>();

    public virtual Engine? Engine { get; set; }
}
