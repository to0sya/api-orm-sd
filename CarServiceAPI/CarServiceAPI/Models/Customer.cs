﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CarServiceAPI.Models;

public partial class Customer : IEntity
{
    public int CustomerId { get; set; }

    public int PersonId { get; set; }

    public string PhoneNumber { get; set; } = null!;

    public virtual ICollection<Ordering> Orderings { get; set; } = new List<Ordering>();

    public virtual Person? Person { get; set; }

    public virtual ICollection<Car> VinCodes { get; set; } = new List<Car>();
}
