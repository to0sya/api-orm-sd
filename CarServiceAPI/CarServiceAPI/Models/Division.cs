﻿using System;
using System.Collections.Generic;

namespace CarServiceAPI.Models;

public partial class Division : IEntity
{
    public int DivisionId { get; set; }

    public string Name { get; set; } = null!;

    public string Location { get; set; } = null!;

    public TimeOnly OpeningTime { get; set; }

    public TimeOnly ClosingTime { get; set; }

    public virtual ICollection<Employee> Employees { get; set; } = new List<Employee>();

    public virtual ICollection<Site> Sites { get; set; } = new List<Site>();
}
