﻿using System;
using System.Collections.Generic;

namespace CarServiceAPI.Models;

public partial class Employee : IEntity
{
    public int EmployeeId { get; set; }

    public int? PersonId { get; set; }

    public int? DivisionId { get; set; }

    public string Position { get; set; } = null!;

    public virtual Division? Division { get; set; }

    public virtual ICollection<OrderItem> OrderItems { get; set; } = new List<OrderItem>();

    public virtual ICollection<Ordering> Orderings { get; set; } = new List<Ordering>();

    public virtual Person? Person { get; set; }

    public virtual ICollection<Site> Sites { get; set; } = new List<Site>();

    public virtual ICollection<Skill> Skills { get; set; } = new List<Skill>();
}
