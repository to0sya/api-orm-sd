﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace CarServiceAPI.Models;

public partial class Engine : IEntity
{
    public int EngineId { get; set; }

    public int Volume { get; set; }

    [Column(name: "aspiration")]
    public Aspiration Aspiration { get; set; }

    [Column(name: "fuel_type")]
    public Fuel Fuel { get; set; }

    public virtual ICollection<CarModel> CarModels { get; set; } = new List<CarModel>();
}
