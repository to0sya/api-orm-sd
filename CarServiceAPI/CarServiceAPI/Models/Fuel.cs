﻿using NpgsqlTypes;

namespace CarServiceAPI.Models
{
    public enum Fuel
    {
        [PgName("Gasoline")]
        Gasoline,
        [PgName("Diesel")]
        Diesel,
        [PgName("Electricity")]
        Electricity,
        [PgName("Gasoline/Propane")]
        GasolinePropane,
        [PgName("Hybrid")]
        Hybrid
    }
}
