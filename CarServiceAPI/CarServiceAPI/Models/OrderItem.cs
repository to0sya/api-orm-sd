﻿using System;
using System.Collections.Generic;

namespace CarServiceAPI.Models;

public partial class OrderItem : IEntity
{
    public int OrderId { get; set; }

    public int ServiceId { get; set; }

    public int? EmployeeId { get; set; }

    public virtual Employee? Employee { get; set; }

    public virtual Ordering? Order { get; set; }

    public virtual Service? Service { get; set; }

    public virtual ICollection<SiteBusyHour> SiteBusyHours { get; set; } = new List<SiteBusyHour>();
}
