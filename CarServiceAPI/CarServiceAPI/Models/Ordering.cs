﻿using System;
using System.Collections.Generic;

namespace CarServiceAPI.Models;

public partial class Ordering : IEntity
{
    public int OrderId { get; set; }

    public string? VinCode { get; set; }

    public int? CustomerId { get; set; }

    public int? ManagerId { get; set; }

    public string? Comment { get; set; }

    public virtual Customer? Customer { get; set; }

    public virtual Employee? Manager { get; set; }

    public virtual ICollection<OrderItem> OrderItems { get; set; } = new List<OrderItem>();

    public virtual Car? VinCodeNavigation { get; set; }
}
