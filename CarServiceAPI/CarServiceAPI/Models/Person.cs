﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CarServiceAPI.Models;

public partial class Person : IEntity
{
    public int PersonId { get; set; }

    public string Name { get; set; } = null!;
    

    public string Surname { get; set; } = null!;

    public DateOnly BirthDate { get; set; }

    
    public string Gender { get; set; } = null!;

    public virtual Customer? Customer { get; set; }

    public virtual Employee? Employee { get; set; }
}
