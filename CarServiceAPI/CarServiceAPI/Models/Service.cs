﻿using System;
using System.Collections.Generic;

namespace CarServiceAPI.Models;

public partial class Service : IEntity
{
    public int ServiceId { get; set; }

    public string Name { get; set; } = null!;

    public decimal Price { get; set; }

    public int? SkillId { get; set; }

    public virtual ICollection<OrderItem> OrderItems { get; set; } = new List<OrderItem>();

    public virtual Skill? Skill { get; set; }
}
