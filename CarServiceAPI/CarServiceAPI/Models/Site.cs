﻿using System;
using System.Collections.Generic;

namespace CarServiceAPI.Models;

public partial class Site : IEntity
{
    public int SiteId { get; set; }

    public int? DivisionId { get; set; }

    public virtual Division? Division { get; set; }

    public virtual ICollection<SiteBusyHour> SiteBusyHours { get; set; } = new List<SiteBusyHour>();

    public virtual ICollection<Employee> Employees { get; set; } = new List<Employee>();
}
