﻿using System;
using System.Collections.Generic;

namespace CarServiceAPI.Models;

public partial class SiteBusyHour : IEntity
{
    public int SiteBusyHoursId { get; set; }

    public int? SiteId { get; set; }

    public DateOnly Date { get; set; }

    public TimeOnly StartTime { get; set; }

    public TimeOnly EndTime { get; set; }

    public int OrderId { get; set; }

    public int ServiceId { get; set; }

    public virtual OrderItem? OrderItem { get; set; }

    public virtual Site? Site { get; set; }
}
