﻿using System;
using System.Collections.Generic;

namespace CarServiceAPI.Models;

public partial class Skill : IEntity
{
    public int SkillId { get; set; }

    public string? Name { get; set; }

    public virtual ICollection<Service> Services { get; set; } = new List<Service>();

    public virtual ICollection<Employee> Employees { get; set; } = new List<Employee>();
}
