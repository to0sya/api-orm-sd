using CarServiceAPI.Contexts;
using CarServiceAPI.Repositories;
using Microsoft.EntityFrameworkCore;
using CarServiceAPI.Models;
using Npgsql;
using Npgsql.NameTranslation;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddCors(options =>
{
    options.AddPolicy("AllowAllOrigins",
        builder =>
        {
            builder
            .AllowAnyOrigin()
            .AllowAnyMethod()
            .AllowAnyHeader();
        });
});

builder.Services.AddControllers().AddNewtonsoftJson();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.AddScoped<DbContext, CarServiceContext>();

#pragma warning disable CS0618 // Type or member is obsolete
NpgsqlConnection.GlobalTypeMapper.MapEnum<Fuel>("fuel_type", new NpgsqlNullNameTranslator());
#pragma warning restore CS0618 // Type or member is obsolete

#pragma warning disable CS0618 // Type or member is obsolete
NpgsqlConnection.GlobalTypeMapper.MapEnum<Aspiration>("aspiration", new NpgsqlNullNameTranslator());
#pragma warning restore CS0618 // Type or member is obsolete

builder.Services.AddScoped<IRepository<Customer, int>,CustomerRepository>();
builder.Services.AddScoped<IRepository<Car, string>,CarRepository>();
builder.Services.AddScoped<IRepository<Employee, int>, EmployeeRepository>();
builder.Services.AddScoped<IRepository<Ordering, int>,  OrderingRepository>();

var app = builder.Build();

app.UseCors("AllowAllOrigins");


AppContext.SetSwitch("Npgsql.EnableStoredProcedureCompatMode", true);

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}


app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
