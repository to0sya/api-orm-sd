﻿using CarServiceAPI.Models;
using Microsoft.EntityFrameworkCore;

namespace CarServiceAPI.Repositories
{
    public class CarRepository : IRepository<Car, string>
    {
        private readonly DbContext _dbContext;

        public CarRepository(DbContext context)
        {
            this._dbContext = context;
        }

        public async Task<string> Add(Car entity)
        {
            var entityEntry = await _dbContext.Set<Car>().AddAsync(entity);
            var answer = await _dbContext.SaveChangesAsync();

            return (answer != 0 ? entityEntry.Entity.VinCode : null) ?? string.Empty;
        }

        public async Task<int> Delete(string vincode)
        {
            var car = await _dbContext.Set<Car>().Where(c => c.VinCode.Equals(vincode)).FirstAsync();

            _dbContext.Set<Car>().Remove(car);
            var answer = await _dbContext.SaveChangesAsync();

            return answer;
        }

        public async Task<Car?> Get(string vincode)
        {
            var answer = await _dbContext.Set<Car>().Where(car => car.VinCode.Equals(vincode))
                                                    .Include(car => car.Model)
                                                    .ThenInclude(model => model!.Engine)
                                                    .FirstOrDefaultAsync();

            return answer;
        }

        public async Task<List<Car>?> GetAll()
        {
            var answer = await _dbContext.Set<Car>()
                .Include(car => car.Model)
                .ThenInclude(model => model!.Engine)
                .ToListAsync();

            return answer;
        }

        public async Task<int> Update(Car entity)
        {
            _dbContext.Entry(entity).State = EntityState.Modified;
            _dbContext.Entry(entity).CurrentValues.SetValues(entity);

            var answer = await _dbContext.SaveChangesAsync();

            return answer;
        }
    }
}
