﻿using CarServiceAPI.Models;
using Microsoft.EntityFrameworkCore;

namespace CarServiceAPI.Repositories
{
    public class CustomerRepository : IRepository<Customer, int>
    {
        private readonly DbContext _dbContext;

        public CustomerRepository(DbContext context)
        {
            this._dbContext = context;
        }

        public async Task<int> Add(Customer entity)
        {
            var entityEntry = await _dbContext.Set<Customer>().AddAsync(entity);
            var answer = await _dbContext.SaveChangesAsync();

            return answer != 0 ? entityEntry.Entity.CustomerId : 0;
        }

        public async Task<int> Delete(int id)
        {
            var customer = await _dbContext.Set<Customer>().Where(c => c.CustomerId.Equals(id)).FirstAsync();

            var person = await _dbContext.Set<Person>().Where(c => c.PersonId.Equals(customer.PersonId)).FirstAsync();
            _dbContext.Set<Person>().Remove(person);
            _dbContext.Set<Customer>().Remove(customer);
            var answer = await _dbContext.SaveChangesAsync();
            
            return answer;
        }

        public async Task<List<Customer>?> GetAll()
        {
            var customersList = await _dbContext.Set<Customer>()
                .Include(c => c.VinCodes)
                .ThenInclude(car => car.Model).ThenInclude(model => model!.Engine)
                .Include(c => c.Person)
                .ToListAsync();

            return customersList;
        }

        public async Task<int> Update(Customer entity)
        {
            var customerEntity = await _dbContext.Set<Customer>().FindAsync(entity.CustomerId);
            if (customerEntity != null)
            {
                var personEntity = await _dbContext.Set<Person>().FindAsync(entity.Person!.PersonId);
                if (personEntity == null)
                {
                    return 0;
                }
                _dbContext.Entry(customerEntity).State = EntityState.Modified;
                _dbContext.Entry(personEntity).State = EntityState.Modified;
                _dbContext.Entry(personEntity).CurrentValues.SetValues(entity.Person);
                _dbContext.Entry(customerEntity).CurrentValues.SetValues(entity.PhoneNumber);
                var answer = await _dbContext.SaveChangesAsync();
                return answer;
            }
            else
            {
                return 0;
            }
        }

        public async Task<Customer?> Get(int id)
        {
            var customer = await _dbContext.Set<Customer>()
                                            .Where(customer => customer.CustomerId.Equals(id))
                                            .Include(c => c.VinCodes)
                                            .ThenInclude(car => car.Model).ThenInclude(model => model!.Engine)
                                            .Include(c => c.Person)
                                            .FirstOrDefaultAsync();

            return customer;
        }
    }
}
