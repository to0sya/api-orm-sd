﻿using CarServiceAPI.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;

namespace CarServiceAPI.Repositories
{
    public class EmployeeRepository : IRepository<Employee, int>
    {
        private readonly DbContext _dbContext;

        public EmployeeRepository(DbContext context)
        {
            this._dbContext = context;
        }

        public async Task<int> Add(Employee entity)
        {
            var skillList = new List<Skill>();
            if (!entity.Skills.IsNullOrEmpty())
            {
                (entity.Skills as List<Skill>)?.ForEach(
                    skill =>
                    {
                        skillList.Add(_dbContext.Set<Skill>().Find(skill.SkillId)!);
                    }
                    );

                entity.Skills = skillList;
            }

            var entityEntry = await _dbContext.Set<Employee>().AddAsync(entity);
            var answer = await _dbContext.SaveChangesAsync();
            
            return answer != 0 ? entityEntry.Entity.EmployeeId : 0;
        }

        public async Task<int> Delete(int id)
        {
            var employee = await _dbContext.Set<Employee>().Where(c => c.EmployeeId.Equals(id)).Include("Person").FirstAsync();
            var person = await _dbContext.Set<Person>().FindAsync(employee!.Person!.PersonId);

            _dbContext.Set<Person>().Remove(person!);
            _dbContext.Set<Employee>().Remove(employee);
            var answer = await _dbContext.SaveChangesAsync();

            return answer;
        }

        public async Task<Employee?> Get(int id)
        {
            var answer = await _dbContext.Set<Employee>()
                                  .Where(employee => employee.EmployeeId.Equals(id))
                                  .Include("Person")
                                  .Include("Skills")
                                  .FirstOrDefaultAsync();

            return answer;
        }

        public async Task<List<Employee>?> GetAll()
        {
            var answer = await _dbContext.Set<Employee>()
                                    .Include("Person")
                                    .Include("Skills")
                                    .ToListAsync();

            return answer;
        }

        public async Task<int> Update(Employee entity)
        {
            _dbContext.Attach(entity);
            _dbContext.Attach(entity.Person!);
            
            _dbContext.Entry(entity).State = EntityState.Modified;
            _dbContext.Entry(entity.Person!).State = EntityState.Modified;

            var answer = await _dbContext.SaveChangesAsync();

            return answer;
        }
    }
}
