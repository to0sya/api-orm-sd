﻿using CarServiceAPI.Models;

namespace CarServiceAPI.Repositories
{
    public interface IRepository<TEntityType, TKeyType> where TEntityType : class, IEntity
    {
        Task<TKeyType> Add(TEntityType entity);

        Task<List<TEntityType>?> GetAll();

        Task<TEntityType?> Get(TKeyType id);

        Task<int> Update(TEntityType entity);

        Task<int> Delete(TKeyType id);
    }
}
