﻿using CarServiceAPI.Models;
using Humanizer;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Newtonsoft.Json;
using NuGet.Configuration;
using System.Net;
using System.Threading.Tasks.Dataflow;

namespace CarServiceAPI.Repositories
{
    public class OrderingRepository : IRepository<Ordering, int>
    {
        private readonly DbContext _dbContext;

        public OrderingRepository(DbContext context)
        {
            this._dbContext = context;
        }

        public async Task<int> Add(Ordering entity)
        {
            var entityEntry = await _dbContext.Set<Ordering>().AddAsync(entity);
            var answer = await _dbContext.SaveChangesAsync();

            return answer != 0 ? entityEntry.Entity.OrderId : 0;
        }

        public async Task<int> Delete(int id)
        {
            var ordering = await _dbContext.Set<Ordering>().FindAsync(id);
            var oiList = await _dbContext.Set<OrderItem>().Where(oi => oi.OrderId == id).Include("SiteBusyHours").ToListAsync();
            var sbhList = oiList.Select(oi => oi.SiteBusyHours).ToList();

            foreach (var sbh in sbhList)
            {
                _dbContext.Set<SiteBusyHour>().Remove(sbh.First());
            }

            foreach (var oi in oiList)
            {
                _dbContext.Set<OrderItem>().Remove(oi);
            }

            _dbContext.Set<Ordering>().Remove(ordering!);
            var answer = await _dbContext.SaveChangesAsync();

            return answer;    
        }

        public async Task<List<Ordering>?> GetAll()
        {
            /*
             * Шкода було видаляти (
             * await (from Ordering in dbContext.Set<Ordering>()
                          select new
                          {
                              car = Ordering.VinCodeNavigation!.BrandName + " " + Ordering.VinCodeNavigation.Model!.ModelName +
                              " " + Ordering.VinCodeNavigation.Model.Engine!.Volume + " " + Ordering.VinCodeNavigation.Model.Engine.Fuel.ToString() +
                              " " + Ordering.VinCodeNavigation.Model.Engine.Aspiration.ToString(),
                              customer = Ordering.Customer!.Person!.Name + " " + Ordering.Customer.Person.Surname,
                              manager = Ordering.Manager!.Person!.Name + " " + Ordering.Manager.Person.Surname,
                              order_items = Ordering.OrderItems.Select(oi => new
                              {
                                  service = oi.Service!.Name,
                                  service_price = oi.Service.Price,
                                  employee = oi.Employee!.Person!.Name + " " + oi.Employee.Person.Surname,
                                  employee_position = oi.Employee.Position,
                                  schedule = oi.SiteBusyHours.Select(sbh => new
                                  {
                                      start_time = sbh.StartTime,
                                      end_time = sbh.EndTime
                                  })

                              })
                          }).ToListAsync();
             */

            var answer = await _dbContext.Set<Ordering>()
                                        .Include(o => o.VinCodeNavigation)
                                        .ThenInclude(vcn => vcn!.Model)
                                        .ThenInclude(model => model!.Engine)
                                        .Include(o => o.Customer)
                                        .ThenInclude(c => c!.Person)
                                        .Include(o => o.Manager)
                                        .ThenInclude(c => c!.Person)
                                        .Include(o => o.OrderItems)
                                        .ThenInclude(oi => oi.Service)
                                        .Include(o => o.OrderItems)
                                        .ThenInclude(oi => oi.Employee)
                                        .ThenInclude(e => e!.Person)
                                        .Include(o => o.OrderItems)
                                        .ThenInclude(oi => oi.SiteBusyHours)
                                        .ToListAsync();
                                        
                                        

            return answer;
        }

        public async Task<int> Update(Ordering entity)
        {
            foreach(OrderItem item in entity.OrderItems)
            {
                _dbContext.Attach(item);
                _dbContext.Entry(item).State = EntityState.Modified;

                foreach(SiteBusyHour sbh in item.SiteBusyHours)
                {
                    _dbContext.Attach(sbh);
                    _dbContext.Entry(sbh).State |= EntityState.Modified;
                }
            }

            _dbContext.Attach(entity);
            _dbContext.Entry(entity).State = EntityState.Modified;
            var answer = await _dbContext.SaveChangesAsync();

            return answer;
        }

        public async Task<Ordering?> Get(int id)
        {
           var answer = await _dbContext.Set<Ordering>()
                            .Where(o => o.OrderId == id)
                            .Include(o => o.VinCodeNavigation)
                            .ThenInclude(vcn => vcn!.Model)
                            .ThenInclude(model => model!.Engine)
                            .Include(o => o.Customer)
                            .ThenInclude(c => c!.Person)
                            .Include(o => o.Manager)
                            .ThenInclude(c => c!.Person)
                            .Include(o => o.OrderItems)
                            .ThenInclude(oi => oi.Service)
                            .Include(o => o.OrderItems)
                            .ThenInclude(oi => oi.Employee)
                            .ThenInclude(e => e!.Person)
                            .Include(o => o.OrderItems)
                            .ThenInclude(oi => oi.SiteBusyHours)
                            .FirstOrDefaultAsync();


           return answer;
        }
    }
}
