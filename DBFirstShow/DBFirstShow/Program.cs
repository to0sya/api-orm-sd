﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace DBFirstShow
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Person person = new Person() { Name = "Anatolii", Surname = "Kochan", 
                                            Contacts = new List<Contacts>() 
                                            { new Contacts() { PhoneNumber = "+380687518246" } } };

            TestDBEntities test = new TestDBEntities();
            test.Person.Add(person);
            test.SaveChanges();
        }
    }
}
